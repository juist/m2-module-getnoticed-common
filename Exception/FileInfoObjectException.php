<?php

namespace GetNoticed\Common\Exception;

use GetNoticed\Common;

class FileInfoObjectException extends Common\Exception\AbstractException
{
    public static function pathIsNotAFileException(string $filePath)
    {
        return new self(__('The given path is not a file: %1', $filePath));
    }

    public static function canNotGatherStats(string $filePath)
    {
        return new self(__('Can not gather statistics from file: %1', $filePath));
    }
}
