<?php

namespace GetNoticed\Common\Exception;

use Magento\Framework;

abstract class AbstractException
    extends Framework\Exception\LocalizedException
{
}