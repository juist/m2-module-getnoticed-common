<?php

namespace GetNoticed\Common\Exception;

class SlugMayNotBeEmptyException
    extends AbstractException
{
}