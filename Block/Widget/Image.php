<?php

namespace GetNoticed\Common\Block\Widget;

use Magento\Framework;
use Magento\Backend;

class Image
    extends Backend\Block\Template
{

    /**
     * @var \Magento\Framework\Data\Form\Element\Factory
     */
    protected $elementFactory;

    public function __construct(
        Backend\Block\Template\Context $context,
        Framework\Data\Form\Element\Factory $elementFactory,
        array $data = []
    ) {
        $this->elementFactory = $elementFactory;
        parent::__construct($context, $data);
    }

    /**
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     *
     * @return \Magento\Framework\Data\Form\Element\AbstractElement
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function prepareElementHtml(Framework\Data\Form\Element\AbstractElement $element)
    {
        // Prepare configuration
        $config = $this->_getData('config');
        $sourceUrl = $this->getUrl(
            'cms/wysiwyg_images/index',
            [
                'target_element_id' => $element->getId(),
                'type'              => 'file'
            ]
        );

        // Create image picker element
        $chooser = $this->getChooserElement($element, $config, $sourceUrl);
        $input = $this->getInputElement($element);
        $element->setData(
            'after_element_html',
            $input->getElementHtml() . $chooser->toHtml() . "<script>require(['mage/adminhtml/browser']);</script>"
        );

        return $element;
    }

    /**
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @param                                                      $config
     * @param                                                      $sourceUrl
     *
     * @return \Magento\Backend\Block\Widget\Button
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getChooserElement(
        Framework\Data\Form\Element\AbstractElement $element,
        $config,
        $sourceUrl
    ): Backend\Block\Widget\Button {
        $chooser = $this->getLayout()->createBlock(Backend\Block\Widget\Button::class)
                        ->setType('button')
                        ->setClass('btn-chooser')
                        ->setLabel($config['button']['open'])
                        ->setOnClick('MediabrowserUtility.openDialog(\'' . $sourceUrl . '\', 0, 0, "MediaBrowser", {})')
                        ->setDisabled($element->getReadonly());

        return $chooser;
    }

    /**
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     *
     * @return \Magento\Framework\Data\Form\Element\AbstractElement
     */
    private function getInputElement(
        Framework\Data\Form\Element\AbstractElement $element
    ): \Magento\Framework\Data\Form\Element\AbstractElement {
        $input = $this->elementFactory->create('text', ['data' => $element->getData()]);
        $input->setId($element->getId());
        $input->setForm($element->getForm());
        $input->setClass('widget-option input-text admin__control-text');

        if ($element->getRequired()) {
            $input->addClass('required-entry');
        }

        return $input;
    }

}
