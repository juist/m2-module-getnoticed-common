<?php

namespace GetNoticed\Common\Helper;

use Magento\Framework\App\Area;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\State;

class AreaHelper extends AbstractHelper
{

    /**
     * @var State
     */
    protected $state;

    public function __construct(
        State $state,
        Context $context
    )
    {
        $this->state = $state;

        parent::__construct($context);
    }

    public function isFrontend()
    {
        return Area::AREA_FRONTEND == $this->state->getAreaCode();
    }

    public function isAdmin()
    {
        return Area::AREA_ADMIN == $this->state->getAreaCode();
    }

    public function isAdminhtml()
    {
        return Area::AREA_ADMINHTML == $this->state->getAreaCode();
    }

    public function isCrontab()
    {
        return Area::AREA_CRONTAB == $this->state->getAreaCode();
    }

}