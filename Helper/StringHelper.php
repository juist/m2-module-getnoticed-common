<?php

namespace GetNoticed\Common\Helper;

use GetNoticed\Common\Exception\SlugMayNotBeEmptyException;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\Exception\LocalizedException;

class StringHelper extends AbstractHelper
{

    const FILTER_MODE_REMOVE_CHARS = 1;
    const FILTER_MODE_ALLOW_CHARS = 2;

    /**
     * @param string $str
     * @param array  $chars
     * @param int    $mode
     */
    public function filter($str, $chars, $mode)
    {
        if ($mode & self::FILTER_MODE_REMOVE_CHARS) {
            // Remove $chars from $str
            $remove = [];

            foreach ($chars as $k => $char) {
                if (is_int($k)) {
                    $remove[$char] = '';
                } else {
                    $remove[$k] = $char;
                }
            }

            return strtr($str, $remove);
        } else {
            if ($mode & self::FILTER_MODE_ALLOW_CHARS) {
                // Remove all but $chars from $str
                $returnStr = '';
                foreach ($this->mb_str_split($str) as $strChar) {
                    if (in_array($strChar, $chars, true)) {
                        $returnStr .= $strChar;
                    }
                }

                return $returnStr;
            } else {
                throw new LocalizedException(__('No valid mode'));
            }
        }
    }

    /**
     * Multibyte version of str_split
     *
     * @param        $str
     * @param string $encoding
     *
     * @return array
     */
    public function mb_str_split($str, $encoding = "UTF-8")
    {
        $strLen = mb_strlen($str);
        $array = [];

        while ($strLen) {
            $array[] = mb_substr($str, 0, 1, $encoding);
            $str = mb_substr($str, 1, $strLen, $encoding);
            $strLen = mb_strlen($str);
        }

        return $array;
    }

    /**
     * @throws \GetNoticed\Common\Exception\SlugMayNotBeEmptyException
     */
    public function slugify(string $text): string
    {
        // Replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // Transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // Remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // Trim
        $text = trim($text, '-');

        // Remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // Lowercase
        $text = strtolower($text);

        if (empty($text)) {
            throw new SlugMayNotBeEmptyException(
                __('Slug may not be empty.')
            );
        }

        return $text;
    }

    public static function camelCase(string $input)
    {
        return strtr(
            ucwords(
                strtolower(
                    strtr($input, ['_' => ' ', '-' => ' '])
                )
            ),
            [
                ' ' => ''
            ]
        );
    }
}
