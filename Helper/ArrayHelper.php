<?php

namespace GetNoticed\Common\Helper;

use Magento\Framework;

/**
 * Class ArrayHelper
 *
 * @package GetNoticed\Common\Helper
 */
class ArrayHelper
    extends Framework\App\Helper\AbstractHelper
{

    // By default checks if key left exists in array right. Can do the other way around as well.
    const FA_CHECK_RTL = 1;
    const FA_DO_FULL_JOIN = 2;

    // What to do if a key is missing?
    const FA_ON_KEY_MISSING_THROW_EXCEPTION = 4;
    const FA_ON_KEY_MISSING_FORCE_CONTINUE = 8; // null will be used

    // Default options
    const FA_DEFAULT = self::FA_CHECK_RTL + self::FA_ON_KEY_MISSING_THROW_EXCEPTION;

    /**
     * @param          $arrayLeft
     * @param          $arrayRight
     * @param callable $forEachItem
     *
     * @return void
     */
    public function foreachArrays(
        $arrayLeft,
        $arrayRight,
        callable $forEachItem,
        $options = self::FA_DEFAULT
    ) {
        // Test some things
        if ($options & self::FA_DO_FULL_JOIN && (false // Here for alignment
                | $options & self::FA_CHECK_RTL
                | $options & self::FA_ON_KEY_MISSING_THROW_EXCEPTION
                | $options & self::FA_ON_KEY_MISSING_FORCE_CONTINUE
            )
        ) {
            throw new \Exception("It's ineffective to use both join and index checkers - please revise your settings.");
        }

        // Check options and perform necessary operations
        if ($options & self::FA_DO_FULL_JOIN) {
            // Full join. Add in right what is missing
            foreach ($arrayLeft as $idx => $arrayLeftRow) {
                if (!array_key_exists($idx, $arrayRight)) {
                    $arrayRight[$idx] = null;
                }
            }

            // Add in left what is missing
            foreach ($arrayRight as $idx => $arrayRightRow) {
                if (!array_key_exists($idx, $arrayLeft)) {
                    $arrayLeft[$idx] = null;
                }
            }
        } elseif ($options & self::FA_CHECK_RTL) {
            // Check right array if required.
            foreach ($arrayRight as $idx => $arrayRightRow) {
                if (!array_key_exists($idx, $arrayLeft)) {
                    if ($options & self::FA_ON_KEY_MISSING_THROW_EXCEPTION) {
                        throw new \Exception(sprintf('foreachArrays: key "%s" missing in left array', $idx));
                    }
                }
            }
        }

        // Loop through arrays as required
        foreach ($arrayLeft as $idx => $arrayLeftRow) {
            if (!array_key_exists($idx, $arrayRight)) {
                if ($options & self::FA_ON_KEY_MISSING_THROW_EXCEPTION) {
                    throw new \Exception(sprintf('foreachArrays: key "%s" missing in right array', $idx));
                } elseif ($options & self::FA_ON_KEY_MISSING_FORCE_CONTINUE) {
                    $arrayRight[$idx] = null;
                } else {
                    continue;
                }
            }

            $forEachItem($idx, $arrayLeft[$idx], $arrayRight[$idx]);
        }
    }

}