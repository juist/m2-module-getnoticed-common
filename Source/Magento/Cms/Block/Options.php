<?php

namespace GetNoticed\Common\Source\Magento\Cms\Block;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magento\Cms\Model\ResourceModel\Block\CollectionFactory as CmsBlockCollectionFactory;

class Options extends AbstractSource implements OptionSourceInterface
{
    /**
     * @var CmsBlockCollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var array
     */
    protected $options;

    public function __construct(
        CmsBlockCollectionFactory $collectionFactory
    ) {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @inheritdoc
     */
    public function getAllOptions()
    {
        if ($this->options === null) {
            // Prepare options
            $this->options = [];
            $this->options[] = [
                'value' => null,
                'label' => __('None')
            ];

            // Fill options
            $collection = $this->collectionFactory->create();

            foreach ($collection as $block) {
                /** @var \Magento\Cms\Model\Block $block */
                $this->options[] = [
                    'value' => $block->getId(),
                    'label' => $block->getTitle()
                ];
            }
        }

        return $this->options;
    }
}
