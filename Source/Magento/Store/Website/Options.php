<?php

namespace GetNoticed\Common\Source\Magento\Store\Website;

use Magento\Framework;
use Magento\Store;

class Options
    implements Framework\Option\ArrayInterface
{

    protected $options;

    // DI

    /**
     * @var Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    public function __construct(
        Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->storeManager = $storeManager;
    }


    public function toOptionArray()
    {
        if ($this->options !== null) {
            return $this->options;
        }

        $this->options = [];

        foreach ($this->storeManager->getWebsites() as $website) {
            $this->options[] = [
                'value' => $website->getId(),
                'label' => $website->getName()
            ];
        }

        return $this->options;
    }

}