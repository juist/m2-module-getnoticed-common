<?php

namespace GetNoticed\Common\Source\Magento\Catalog\Category;

use Magento\Framework;
use Magento\Catalog;

class Options
    implements Framework\Option\ArrayInterface
{

    protected $options;

    protected $categoryDepth = 0;

    // DI

    /**
     * @var Catalog\Model\ResourceModel\Category\CollectionFactory
     */
    protected $categoryCollectionFactory;

    public function __construct(
        Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory
    ) {
        $this->categoryCollectionFactory = $categoryCollectionFactory;
    }

    public function toOptionArray()
    {
        if ($this->options !== null) {
            return $this->options;
        }

        $this->options = [];

        // Fetch categories and fill options
        foreach ($this->getRootCategoriesByParentId() as $category) {
            $this->options[] = [
                'value' => $category->getId(),
                'label' => $category->getName()
            ];

            $this->recursivelyAddCategoryChildren($category);
        }

        return $this->options;
    }

    private function recursivelyAddCategoryChildren(
        Catalog\Model\Category $category
    ) {
        $this->categoryDepth++;

        foreach ($this->getRootCategoriesByParentId($category->getId()) as $childCategory) {
            $this->options[] = [
                'value' => $childCategory->getId(),
                'label' => str_repeat('- ', $this->categoryDepth) . $childCategory->getName()
            ];

            $this->recursivelyAddCategoryChildren($childCategory);
        }

        $this->categoryDepth--;
    }

    /**
     * @param int $parentId
     *
     * @return \Magento\Catalog\Model\ResourceModel\Category\Collection
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getRootCategoriesByParentId(int $parentId = 0): Catalog\Model\ResourceModel\Category\Collection
    {
        $categories = $this->getNewCategoryCollection();
        $categories
            ->addAttributeToSelect('*')
            ->addFieldToFilter('parent_id', ['eq' => $parentId]);

        return $categories;
    }

    private function getNewCategoryCollection(): Catalog\Model\ResourceModel\Category\Collection
    {
        return $this->categoryCollectionFactory->create();
    }

}