<?php

namespace GetNoticed\Common\Source\Magento\Catalog\RootCatalog;

use Magento\Framework;
use Magento\Catalog;

class Options
    implements Framework\Option\ArrayInterface
{

    protected $options;

    // DI

    /**
     * @var Catalog\Model\ResourceModel\Category\CollectionFactory
     */
    protected $categoryCollectionFactory;

    public function __construct(
        Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory
    ) {
        $this->categoryCollectionFactory = $categoryCollectionFactory;
    }

    public function toOptionArray()
    {
        if ($this->options !== null) {
            return $this->options;
        }

        $this->options = [];

        foreach ($this->getRootCategories() as $rootCategory) {
            $this->options[] = [
                'value' => $rootCategory->getId(),
                'label' => $rootCategory->getName()
            ];
        }

        return $this->options;
    }

    /**
     * @return \Magento\Catalog\Model\Category[]
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getRootCategories()
    {
        $rootCategories = $this->categoryCollectionFactory->create();
        $rootCategories
            ->addAttributeToSelect('*')
            ->addFieldToFilter('parent_id', ['eq' => '1']);

        return $rootCategories;
    }

}