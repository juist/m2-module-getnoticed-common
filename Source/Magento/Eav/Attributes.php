<?php

namespace GetNoticed\Common\Source\Magento\Eav;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\Eav\Model\Config as EavConfig;
use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\CollectionFactory as EavAttributeCollectionFactory;
use Magento\Catalog\Api\Data\ProductAttributeInterface;

class Attributes extends AbstractSource implements OptionSourceInterface
{
    /**
     * @var EavAttributeCollectionFactory
     */
    protected $attributeCollectionFactory;

    /**
     * @var EavConfig
     */
    protected $eavConfig;

    /**
     * @var array
     */
    protected $options;

    public function __construct(EavAttributeCollectionFactory $attributeCollectionFactory, EavConfig $eavConfig)
    {
        $this->attributeCollectionFactory = $attributeCollectionFactory;
        $this->eavConfig = $eavConfig;
    }

    public function getAllOptions()
    {
        if ($this->options === null) {
            $this->options = [];

            // Load entity
            $productEntity = $this->eavConfig->getEntityType(ProductAttributeInterface::ENTITY_TYPE_CODE);

            /** @var Eav\Model\ResourceModel\Entity\Attribute\Collection $attributeCollection */
            $attributeCollection = $this->attributeCollectionFactory->create();
            $attributeCollection->addFieldToFilter('entity_type_id', ['eq' => $productEntity->getId()]);

            foreach ($attributeCollection as $attribute) {
                /** @var Eav\Model\Entity\Attribute $attribute */
                $this->options[] = [
                    'value' => $attribute->getId(),
                    'label' => sprintf('%s (%s)', $attribute->getDefaultFrontendLabel(), $attribute->getName())
                ];
            }
        }

        return $this->options;
    }
}
