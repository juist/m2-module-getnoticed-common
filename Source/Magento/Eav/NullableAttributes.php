<?php

namespace GetNoticed\Common\Source\Magento\Eav;

class NullableAttributes extends Attributes
{
    public function getAllOptions()
    {
        return array_merge(
            [
                ['value' => '', 'label' => __('-- Select an attribute --')]
            ],
            parent::getAllOptions()
        );
    }
}
