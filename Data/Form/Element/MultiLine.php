<?php

namespace GetNoticed\Common\Data\Form\Element;

/**
 * Class MultiLine
 *
 * @package GetNoticed\Common\Data\Form\Element
 */
class MultiLine
    extends \Magento\Framework\Data\Form\Element\Multiline
{

    /**
     * MultiLine constructor.
     *
     * @param \Magento\Framework\Data\Form\Element\Factory           $factoryElement
     * @param \Magento\Framework\Data\Form\Element\CollectionFactory $factoryCollection
     * @param \Magento\Framework\Escaper                             $escaper
     * @param array                                                  $data
     * @param int                                                    $lineCount
     */
    public function __construct(
        \Magento\Framework\Data\Form\Element\Factory $factoryElement,
        \Magento\Framework\Data\Form\Element\CollectionFactory $factoryCollection,
        \Magento\Framework\Escaper $escaper,
        array $data = [],
        int $lineCount = 2
    ) {
        parent::__construct($factoryElement, $factoryCollection, $escaper, $data);
        $this->setLineCount($lineCount);
    }

    /**
     * @param string $index
     *
     * @return null
     */
    public function getEscapedValue($index = null)
    {
        $value = explode(',', $this->getData('value'));

        if (array_key_exists($index, $value)) {
            $data = $value[$index];

            if ($filter = $this->getData('value_filter')) {
                $data = $filter->filter($data);
            }

            return $this->_escape($data);
        }

        return null;
    }

}
