<?php

namespace GetNoticed\Common\Data\Form\Element\Select;

use Magento\Framework;

/**
 * Class Options
 *
 * @package GetNoticed\Common\Data\Form\Element\Select
 */
class Options
    implements Framework\Option\ArrayInterface,
               ValidateOptionsInterface
{

    /**
     * @var array
     */
    protected $options;

    /**
     * @inheritDoc
     */
    public function __construct($options = [])
    {
        $this->options = $options;

        $this->validateOptions();
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function validateOptions()
    {
        if (!is_array($this->options)) {
            throw new Framework\Exception\LocalizedException(__('Options must be an array.'));
        }

        foreach ($this->options as $idx => $option) {
            if (!is_array($option)) {
                throw new Framework\Exception\LocalizedException(__('Option "%1" must be an array', $idx));
            }

            if (!array_key_exists('value', $option) || !array_key_exists('label', $option)) {
                throw new Framework\Exception\LocalizedException(
                    __('Option "%1" is missing a value and/or label', $idx)
                );
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function toOptionArray()
    {
        return $this->options;
    }

    /**
     * @inheritDoc
     */
    public function isValidOptionByValue(string $value): bool
    {
        foreach ($this->options as $option) {
            if ($option['value'] === $value) {
                return true;
            }
        }

        return false;
    }

}