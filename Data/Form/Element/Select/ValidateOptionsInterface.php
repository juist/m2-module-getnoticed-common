<?php

namespace GetNoticed\Common\Data\Form\Element\Select;

/**
 * Interface ValidateOptionsInterface
 *
 * @package GetNoticed\Common\Data\Form\Element\Select
 */
interface ValidateOptionsInterface
{

    /**
     * @param string $value
     *
     * @return bool
     */
    public function isValidOptionByValue(string $value): bool;

}