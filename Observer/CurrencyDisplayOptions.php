<?php

namespace GetNoticed\Common\Observer;

use GetNoticed\Common\Helper\AreaHelper;
use Magento\Framework\DataObject;
use Magento\Framework\Event\ObserverInterface;

class CurrencyDisplayOptions implements ObserverInterface
{

    /**
     * @var AreaHelper
     */
    protected $areaHelper;

    public function __construct(
        AreaHelper $areaHelper
    )
    {
        $this->areaHelper = $areaHelper;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if (!$this->areaHelper->isFrontend()) {
            return $this;
        }

        /** @var string $baseCode */
        $baseCode = $observer->getEvent()->getData('base_code');
        /** @var DataObject $currencyOptions */
        $currencyOptions = $observer->getEvent()->getData('currency_options');

        if ('EUR' === $baseCode) {
            $currencyOptions->setData('format', 'de');
            $currencyOptions->setData('position', \Magento\Framework\Currency::LEFT);
        }

        return $this;
    }

}