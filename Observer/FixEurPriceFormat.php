<?php

namespace GetNoticed\Common\Observer;

use GetNoticed\Common\Helper\AreaHelper;
use Magento\Framework\DataObject;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class FixEurPriceFormat
 * @package GetNoticed\Common\Observer
 */
class FixEurPriceFormat implements ObserverInterface
{

    /**
     * @var AreaHelper
     */
    protected $areaHelper;

    public function __construct(
        AreaHelper $areaHelper
    )
    {
        $this->areaHelper = $areaHelper;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if (!$this->areaHelper->isFrontend()) {
            return $this;
        }

        /** @var DataObject $priceFormat */
        $priceFormat = $observer->getData('price_format');
        $format = $priceFormat->getData('format');
        /** @var \Magento\Directory\Model\Currency $currency */
        $currency = $observer->getData('currency');

        // If currency is EUR, fix the group/decimal symbols
        if ($currency && 'EUR' == $currency->getCode()) {
            $format['decimalSymbol'] = ',';
            $format['groupSymbol'] = '.';
        }

        $priceFormat->setData('format', $format);
        $observer->setData('price_format', $priceFormat);

        return $this;
    }

}