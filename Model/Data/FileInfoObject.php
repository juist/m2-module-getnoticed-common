<?php

namespace GetNoticed\Common\Model\Data;

use GetNoticed\Common;
use Magento\Framework;

class FileInfoObject
{
    /**
     * @var string
     */
    private $directoryName;

    /**
     * @var string
     */
    private $baseName;

    /**
     * @var string|null
     */
    private $extension;

    /**
     * @var string
     */
    private $fileName;

    /**
     * device number
     *
     * @var int
     */
    private $deviceNumber;

    /**
     * inode number
     *
     * @var int
     */
    private $inodeNumber;

    /**
     * inode protection mode
     *
     * @var int
     */
    private $inodeMode;

    /**
     * number of links
     *
     * @var int
     */
    private $numberOfLinks;

    /**
     * userid of owner
     *
     * @var int
     */
    private $ownerUserId;

    /**
     * groupid of owner
     *
     * @var int
     */
    private $ownerGroupId;

    /**
     * @var int
     */
    private $deviceType;

    /**
     * size in bytes
     *
     * @var int
     */
    private $size;

    /**
     * time of last access (Unix timestamp)
     *
     * @var int
     */
    private $lastAccessedTime;

    /**
     * time of last modification (Unix timestamp)
     *
     * @var int
     */
    private $lastModifiedTime;

    /**
     * time of last inode change (Unix timestamp)
     *
     * @var int
     */
    private $createdAtTime;

    /**
     * blocksize of filesystem IO
     *
     * @var int
     */
    private $blockSize;

    /**
     * number of 512-byte blocks allocated
     *
     * @var int
     */
    private $blocks;

    // DI

    /**
     * @var Framework\Filesystem\DriverInterface|Framework\Filesystem\Driver\File
     */
    private $fileDriver;

    public function __construct(
        Framework\Filesystem\DriverPool $driverPool
    ) {
        $this->fileDriver = $driverPool->getDriver(Framework\Filesystem\DriverPool::FILE);
    }

    // Generating methods

    /**
     * @return string
     */
    public function getRealPath(): string
    {
        return $this->getDirectoryName() . DIRECTORY_SEPARATOR . $this->getBaseName();
    }

    /**
     * @return string
     * @throws Framework\Exception\FileSystemException
     */
    public function getContents(): string
    {
        return $this->fileDriver->fileGetContents($this->getRealPath());
    }

    // Getters / Setters

    /**
     * @return string
     */
    public function getDirectoryName(): string
    {
        return $this->directoryName;
    }

    /**
     * @param string $directoryName
     *
     * @return FileInfoObject
     */
    public function setDirectoryName(string $directoryName): Common\Model\Data\FileInfoObject
    {
        $this->directoryName = $directoryName;

        return $this;
    }

    /**
     * @return string
     */
    public function getBaseName(): string
    {
        return $this->baseName;
    }

    /**
     * @param string $baseName
     *
     * @return FileInfoObject
     */
    public function setBaseName(string $baseName): Common\Model\Data\FileInfoObject
    {
        $this->baseName = $baseName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getExtension(): ?string
    {
        return $this->extension;
    }

    /**
     * @param string|null $extension
     *
     * @return FileInfoObject
     */
    public function setExtension(?string $extension): Common\Model\Data\FileInfoObject
    {
        $this->extension = $extension;

        return $this;
    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     *
     * @return FileInfoObject
     */
    public function setFileName(string $fileName): Common\Model\Data\FileInfoObject
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * @return int
     */
    public function getDeviceNumber(): int
    {
        return $this->deviceNumber;
    }

    /**
     * @param int $deviceNumber
     *
     * @return FileInfoObject
     */
    public function setDeviceNumber(int $deviceNumber): Common\Model\Data\FileInfoObject
    {
        $this->deviceNumber = $deviceNumber;

        return $this;
    }

    /**
     * @return int
     */
    public function getInodeNumber(): int
    {
        return $this->inodeNumber;
    }

    /**
     * @param int $inodeNumber
     *
     * @return FileInfoObject
     */
    public function setInodeNumber(int $inodeNumber): Common\Model\Data\FileInfoObject
    {
        $this->inodeNumber = $inodeNumber;

        return $this;
    }

    /**
     * @return int
     */
    public function getInodeMode(): int
    {
        return $this->inodeMode;
    }

    /**
     * @param int $inodeMode
     *
     * @return FileInfoObject
     */
    public function setInodeMode(int $inodeMode): Common\Model\Data\FileInfoObject
    {
        $this->inodeMode = $inodeMode;

        return $this;
    }

    /**
     * @return int
     */
    public function getNumberOfLinks(): int
    {
        return $this->numberOfLinks;
    }

    /**
     * @param int $numberOfLinks
     *
     * @return FileInfoObject
     */
    public function setNumberOfLinks(int $numberOfLinks): Common\Model\Data\FileInfoObject
    {
        $this->numberOfLinks = $numberOfLinks;

        return $this;
    }

    /**
     * @return int
     */
    public function getOwnerUserId(): int
    {
        return $this->ownerUserId;
    }

    /**
     * @param int $ownerUserId
     *
     * @return FileInfoObject
     */
    public function setOwnerUserId(int $ownerUserId): Common\Model\Data\FileInfoObject
    {
        $this->ownerUserId = $ownerUserId;

        return $this;
    }

    /**
     * @return int
     */
    public function getOwnerGroupId(): int
    {
        return $this->ownerGroupId;
    }

    /**
     * @param int $ownerGroupId
     *
     * @return FileInfoObject
     */
    public function setOwnerGroupId(int $ownerGroupId): Common\Model\Data\FileInfoObject
    {
        $this->ownerGroupId = $ownerGroupId;

        return $this;
    }

    /**
     * @return int
     */
    public function getDeviceType(): int
    {
        return $this->deviceType;
    }

    /**
     * @param int $deviceType
     *
     * @return FileInfoObject
     */
    public function setDeviceType(int $deviceType): Common\Model\Data\FileInfoObject
    {
        $this->deviceType = $deviceType;

        return $this;
    }

    /**
     * @return int
     */
    public function getSize(): int
    {
        return $this->size;
    }

    /**
     * @param int $size
     *
     * @return FileInfoObject
     */
    public function setSize(int $size): Common\Model\Data\FileInfoObject
    {
        $this->size = $size;

        return $this;
    }

    /**
     * @return int
     */
    public function getLastAccessedTime(): int
    {
        return $this->lastAccessedTime;
    }

    /**
     * @param int $lastAccessedTime
     *
     * @return FileInfoObject
     */
    public function setLastAccessedTime(int $lastAccessedTime): Common\Model\Data\FileInfoObject
    {
        $this->lastAccessedTime = $lastAccessedTime;

        return $this;
    }

    /**
     * @return int
     */
    public function getLastModifiedTime(): int
    {
        return $this->lastModifiedTime;
    }

    /**
     * @param int $lastModifiedTime
     *
     * @return FileInfoObject
     */
    public function setLastModifiedTime(int $lastModifiedTime): Common\Model\Data\FileInfoObject
    {
        $this->lastModifiedTime = $lastModifiedTime;

        return $this;
    }

    /**
     * @return int
     */
    public function getCreatedAtTime(): int
    {
        return $this->createdAtTime;
    }

    /**
     * @param int $createdAtTime
     *
     * @return FileInfoObject
     */
    public function setCreatedAtTime(int $createdAtTime): Common\Model\Data\FileInfoObject
    {
        $this->createdAtTime = $createdAtTime;

        return $this;
    }

    /**
     * @return int
     */
    public function getBlockSize(): int
    {
        return $this->blockSize;
    }

    /**
     * @param int $blockSize
     *
     * @return FileInfoObject
     */
    public function setBlockSize(int $blockSize): Common\Model\Data\FileInfoObject
    {
        $this->blockSize = $blockSize;

        return $this;
    }

    /**
     * @return int
     */
    public function getBlocks(): int
    {
        return $this->blocks;
    }

    /**
     * @param int $blocks
     *
     * @return FileInfoObject
     */
    public function setBlocks(int $blocks): Common\Model\Data\FileInfoObject
    {
        $this->blocks = $blocks;

        return $this;
    }
}
