<?php

namespace GetNoticed\Common\Model\Data;

use GetNoticed\Common;
use Magento\Framework;

class FileInfoObjectFactory implements Framework\ObjectManager\FactoryInterface
{
    const DEFAULT_CLASS = Common\Model\Data\FileInfoObject::class;

    /**
     * @var Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var Framework\Filesystem\Io\File
     */
    private $fileIo;

    /**
     * @var Framework\Filesystem\DriverInterface
     */
    private $fileDriver;

    public function __construct(
        Framework\Filesystem\Io\File $fileIo,
        Framework\Filesystem\DriverInterface $fileDriver
    ) {
        $this->fileIo = $fileIo;
        $this->fileDriver = $fileDriver;
        $this->objectManager = Framework\App\ObjectManager::getInstance();
    }

    /**
     * @inheritDoc
     * @return object|Common\Model\Data\FileInfoObject
     */
    public function create($requestedType = self::DEFAULT_CLASS, array $arguments = [])
    {
        if (\is_subclass_of($requestedType, Common\Model\Data\FileInfoObject::class, true)) {
            throw new Framework\Exception\LocalizedException(
                __('Object must implement/extend %1', Common\Model\Data\FileInfoObject::class)
            );
        }

        return $this->objectManager->create($requestedType, $arguments);
    }

    /**
     * @param string $filePath
     *
     * @return \GetNoticed\Common\Model\Data\FileInfoObject
     * @throws \GetNoticed\Common\Exception\FileInfoObjectException
     */
    public function createFromPath(string $filePath)
    {
        if ($this->fileDriver->isFile($filePath) !== true) {
            throw Common\Exception\FileInfoObjectException::pathIsNotAFileException($filePath);
        }

        try {
            $pathInfo = $this->fileIo->getPathInfo($filePath);
            $stat = $this->fileDriver->stat($filePath);
        } catch (Framework\Exception\FileSystemException $e) {
            throw Common\Exception\FileInfoObjectException::canNotGatherStats($filePath);
        }

        if (is_array($pathInfo) !== true || is_array($stat) !== true) {
            throw Common\Exception\FileInfoObjectException::canNotGatherStats($filePath);
        }

        if (array_key_exists('extension', $pathInfo) === false) {
            $pathInfo['extension'] = null;
        }

        return $this->create()
                    ->setDirectoryName($pathInfo['dirname'])
                    ->setBaseName($pathInfo['basename'])
                    ->setFileName($pathInfo['filename'])
                    ->setExtension($pathInfo['extension'])
                    ->setDeviceNumber($stat['dev'])
                    ->setInodeNumber($stat['ino'])
                    ->setInodeMode($stat['mode'])
                    ->setNumberOfLinks($stat['nlink'])
                    ->setOwnerUserId($stat['uid'])
                    ->setOwnerGroupId($stat['gid'])
                    ->setDeviceType($stat['rdev'])
                    ->setSize($stat['size'])
                    ->setLastAccessedTime($stat['atime'])
                    ->setLastModifiedTime($stat['mtime'])
                    ->setCreatedAtTime($stat['ctime'])
                    ->setBlockSize($stat['blksize'])
                    ->setBlocks($stat['blocks']);
    }

    /**
     * @inheritDoc
     */
    public function setObjectManager(Framework\ObjectManagerInterface $objectManager)
    {
        return $this->objectManager = $objectManager;
    }
}