<?php

namespace GetNoticed\Common\Task;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

interface TaskInterface
{

    /**
     * Does whatever this task should do.
     *
     * @throws \Exception
     * @return mixed
     */
    public function run();

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return TaskInterface
     */
    public function setConsoleStreams(InputInterface $input, OutputInterface $output);

}