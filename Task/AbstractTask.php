<?php

namespace GetNoticed\Common\Task;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

abstract class AbstractTask implements TaskInterface
{

    /** @var InputInterface */
    protected $input = null;

    /** @var OutputInterface */
    protected $output = null;

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return $this
     */
    public function setConsoleStreams(InputInterface $input, OutputInterface $output)
    {
        $this->input = $input;
        $this->output = $output;

        return $this;
    }

}