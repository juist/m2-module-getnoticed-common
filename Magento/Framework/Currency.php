<?php

namespace GetNoticed\Common\Magento\Framework;

use GetNoticed\Common\Helper\AreaHelper;
use Magento\Framework\App\CacheInterface;

/**
 * Class Currency
 * @package GetNoticed\Common\Magento\Framework
 */
class Currency extends \Magento\Framework\Currency
{

    /**
     * @var AreaHelper
     */
    protected $areaHelper;

    public function __construct(
        AreaHelper $areaHelper,
        CacheInterface $appCache,
        $options = null,
        $locale = null
    )
    {
        $this->areaHelper = $areaHelper;

        parent::__construct($appCache, $options, $locale);
    }

    /**
     * @param null $value
     * @param array $options
     * @return string
     */
    public function toCurrency($value = null, array $options = []): string
    {
        if (!$this->areaHelper->isFrontend()) {
            return parent::toCurrency($value, $options);
        }

        if (!array_key_exists('format', $options)) {
            $options['format'] = 'de';
        }

        if (!array_key_exists('position', $options)) {
            $options['position'] = self::LEFT;
        }

        return parent::toCurrency($value, $options);
    }

}