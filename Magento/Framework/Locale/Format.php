<?php

namespace GetNoticed\Common\Magento\Framework\Locale;

use GetNoticed\Common\Helper\AreaHelper;
use Magento\Framework\DataObjectFactory;
use Magento\Framework\Locale\ResolverInterface;

/**
 * Class Format
 * @package GetNoticed\Common\Magento\Framework\Locale
 * @see \Magento\Framework\Locale\Format
 */
class Format
{

    /**
     * @var \Magento\Framework\App\ScopeResolverInterface
     */
    protected $scopeResolver;

    /**
     * @var \Magento\Framework\Locale\ResolverInterface
     */
    protected $localeResolver;

    /**
     * @var \Magento\Directory\Model\CurrencyFactory
     */
    protected $currencyFactory;

    /**
     * @var \Magento\Framework\Event\Manager
     */
    protected $eventManager;

    /**
     * @var DataObjectFactory
     */
    protected $dataObjectFactory;

    /**
     * @var AreaHelper
     */
    protected $areaHelper;

    /**
     * @param \Magento\Framework\App\ScopeResolverInterface $scopeResolver
     * @param ResolverInterface $localeResolver
     * @param \Magento\Directory\Model\CurrencyFactory $currencyFactory
     */
    public function __construct(
        AreaHelper $areaHelper,
        \Magento\Framework\App\ScopeResolverInterface $scopeResolver,
        \Magento\Framework\Locale\ResolverInterface $localeResolver,
        \Magento\Directory\Model\CurrencyFactory $currencyFactory,
        \Magento\Framework\Event\Manager $eventManager,
        DataObjectFactory $dataObjectFactory
    )
    {
        $this->areaHelper = $areaHelper;
        $this->scopeResolver = $scopeResolver;
        $this->localeResolver = $localeResolver;
        $this->currencyFactory = $currencyFactory;
        $this->eventManager = $eventManager;
        $this->dataObjectFactory = $dataObjectFactory;
    }

    /**
     * @param \Magento\Framework\Locale\Format\Interceptor $formatInterceptor
     * @param \Closure $closure
     * @param null $localeCode
     * @param null $currencyCode
     * @return mixed
     */
    public function aroundGetPriceFormat(
        \Magento\Framework\Locale\Format\Interceptor $formatInterceptor,
        \Closure $closure,
        $localeCode = null,
        $currencyCode = null
    )
    {
        // Original return value
        $priceFormat = $closure($localeCode, $currencyCode);

        // Only trigger on frontend
        if (!$this->areaHelper->isFrontend()) {
            return $priceFormat;
        }

        // Check locale & currency requested
        $localeCode = $localeCode ?: $this->localeResolver->getLocale();
        if ($currencyCode) {
            $currency = $this->currencyFactory->create()->load($currencyCode);
        } else {
            $currency = $this->scopeResolver->getScope()->getCurrentCurrency();
        }

        // Add in an event to allow changes to be made
        $priceFormatCarrier = $this->dataObjectFactory->create();
        $priceFormatCarrier->setData('format', $priceFormat);
        $this->eventManager->dispatch('getnoticed_common_after_get_price_format', [
            'price_format' => $priceFormatCarrier,
            'locale_code'  => $localeCode,
            'currency'     => $currency
        ]);

        // Return
        return $priceFormatCarrier->getData('format');
    }

}