<?php

namespace GetNoticed\Common\Console\Command;

use Psr\Log;
use Symfony\Component\Console;

abstract class AbstractCommand extends Console\Command\Command implements Log\LoggerInterface
{
    /**
     * Default separator used by _optionToArray
     */
    const DEFAULT_OPTION_TO_ARRAY_SEPARATOR = ',';

    /**
     * @see https://symfony.com/doc/current/console/coloring.html
     */
    const OUTPUT_FORMATS = [
        self::DEBUG     => '<comment>%s</comment>',
        self::INFO      => '<comment>%s</comment>',
        self::NOTICE    => '<comment>%s</comment>',
        self::WARNING   => '<comment>%s</comment>',
        self::ERROR     => '<error>%s</error>',
        self::CRITICAL  => '<error>%s</error>',
        self::ALERT     => '<comment>%s</comment>',
        self::EMERGENCY => '<error>%s</error>'
    ];

    /**
     * Detailed debug information
     */
    const DEBUG = 100;

    /**
     * Interesting events
     *
     * Examples: User logs in, SQL logs.
     */
    const INFO = 200;

    /**
     * Uncommon events
     */
    const NOTICE = 250;

    /**
     * Exceptional occurrences that are not errors
     *
     * Examples: Use of deprecated APIs, poor use of an API,
     * undesirable things that are not necessarily wrong.
     */
    const WARNING = 300;

    /**
     * Runtime errors
     */
    const ERROR = 400;

    /**
     * Critical conditions
     *
     * Example: Application component unavailable, unexpected exception.
     */
    const CRITICAL = 500;

    /**
     * Action must be taken immediately
     *
     * Example: Entire website down, database unavailable, etc.
     * This should trigger the SMS alerts and wake you up.
     */
    const ALERT = 550;

    /**
     * Urgent alert.
     */
    const EMERGENCY = 600;

    /**
     * @var Console\Input\InputInterface
     */
    protected $input;

    /**
     * @var Console\Output\OutputInterface
     */
    protected $output;

    /**
     * @param string $message
     * @param array  $context
     */
    public function emergency(string|\Stringable $message, array $context = []): void
    {
        $this->log(self::EMERGENCY, $message, $context);
    }

    /**
     * @param string $message
     * @param array  $context
     */
    public function alert(string|\Stringable $message, array $context = []): void
    {
        $this->log(self::ALERT, $message, $context);
    }

    /**
     * @param string $message
     * @param array  $context
     */
    public function critical(string|\Stringable $message, array $context = []): void
    {
        $this->log(self::CRITICAL, $message, $context);
    }

    /**
     * @param string $message
     * @param array  $context
     */
    public function error(string|\Stringable $message, array $context = []): void
    {
        $this->log(self::ERROR, $message, $context);
    }

    /**
     * @param string $message
     * @param array  $context
     */
    public function warning(string|\Stringable $message, array $context = []): void
    {
        $this->log(self::WARNING, $message, $context);
    }

    /**
     * @param string $message
     * @param array  $context
     */
    public function notice(string|\Stringable $message, array $context = []): void
    {
        $this->log(self::NOTICE, $message, $context);
    }

    /**
     * @param string $message
     * @param array  $context
     */
    public function info(string|\Stringable $message, array $context = []): void
    {
        $this->log(self::INFO, $message, $context);
    }

    /**
     * @param string $message
     * @param array  $context
     */
    public function debug(string|\Stringable $message, array $context = []): void
    {
        $this->log(self::DEBUG, $message, $context);
    }

    /**
     * @param mixed  $level
     * @param string $message
     * @param array  $context
     */
    public function log($level, string|\Stringable $message, array $context = []): void
    {
        $this->output->writeln(sprintf($this->getOutputFormat($level), $message));
    }

    /**
     * @param int $level
     *
     * @return string
     */
    private function getOutputFormat(int $level)
    {
        if (in_array($level, self::OUTPUT_FORMATS) === true) {
            return self::OUTPUT_FORMATS[$level];
        } else {
            // No special formatting
            return '%s';
        }
    }

    /**
     * Given the (option) value and optionally a separator, this function will return an array of options specified.
     *
     * @param string $optionValue
     * @param string $separator
     *
     * @return array
     */
    protected function _optionToArray($optionValue, $separator = self::DEFAULT_OPTION_TO_ARRAY_SEPARATOR)
    {
        $optionValue = trim($optionValue);

        if (strlen($optionValue) > 0) {
            $data = explode($separator, $optionValue);

            foreach ($data as $k => &$v) {
                $v = trim($v);

                if (strlen($v) < 1) {
                    unset($data[$k]);
                }
            }

            return $data;
        }

        return [];
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     */
    protected function execute(Console\Input\InputInterface $input, Console\Output\OutputInterface $output)
    {
        $this->input = $input;
        $this->output = $output;
    }

}
