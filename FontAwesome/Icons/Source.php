<?php

namespace GetNoticed\Common\FontAwesome\Icons;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class Source
 * @package GetNoticed\Common\FontAwesome\Icons
 */
class Source implements ArrayInterface
{

    /**
     * @var \Magento\Framework\Module\Dir\Reader
     */
    protected $moduleDirReader;

    /**
     * @var \Magento\Framework\Xml\Parser
     */
    private $xmlParser;

    /**
     * Source constructor.
     */
    public function __construct(
        \Magento\Framework\Module\Dir\Reader $moduleDirReader,
        \Magento\Framework\Xml\Parser $xmlParser
    )
    {
        $this->moduleDirReader = $moduleDirReader;
        $this->xmlParser = $xmlParser;
    }

    public function loadIconOptionsFromXml()
    {
        // Load from XML
        $filePath = $this->moduleDirReader->getModuleDir('etc', 'GetNoticed_Common') . '/fa-icons.xml';
        $parsedArray = $this->xmlParser->load($filePath)->xmlToArray();

        // Fix some parsing errors
        $iconOptions = [];
        foreach ($parsedArray['config']['_value']['icons']['icon'] ?: [] as $options) {
            if (!is_array($options)) {
                $options = [$options];

                foreach ($options as $option) {
                    $iconOptions[] = $option;
                }
            }
        }

        return $iconOptions;
    }

    /**
     * {@inheritdoc}
     */
    public function toOptionArray()
    {
        $options = $this->loadIconOptionsFromXml();
        $optionArray = [];

        sort($options, SORT_STRING + SORT_NATURAL);

        foreach ($options as $iconClass) {
            $optionArray[] = [
                'value' => $iconClass,
                'label' => $iconClass
            ];
        }

        return $optionArray;
    }

}