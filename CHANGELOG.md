# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.3.0] - 2017-10-25
### Added
* PHON-98 : Start of this changelog file
* PHON-67 : Add the developer module as dev dependency
* PHON-67 : Add a MultiLine element which extends Magento's core MultiLine element and allow changing constructor arguments

### Updated
* PHON-98 : Updated the AbstractCommand class to implement the LoggerInterface. It can now be used as a logger.
  E.g., when you're running a task and it uses a logger to log, it can use the command to output information.
